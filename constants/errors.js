exports.INTERNAL_SERVER_ERROR = "Internal Server Error";
exports.BAD_REQUEST = "Bad request";
exports.NAME_LENGTH = "Name length between 1 and 255";
exports.ID_INT = "The ID must be an integer greater than 1";
exports.OBJECT_NOT_FOUND = "Object not found";
exports.OBJECT_NOT_UPDATED = "Object not updated";
exports.OBJECT_NOT_DELETED = "Object not deleted";