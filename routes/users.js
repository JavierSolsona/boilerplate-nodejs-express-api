var express = require('express');
var router = express.Router();
const UsersController = require('../controllers/users-controller');

router.get('/', UsersController.index);
router.post('/', UsersController.validate('create'), UsersController.create);
router.get('/:userId', UsersController.validate('urlParameter'), UsersController.getById);
router.put('/:userId', UsersController.validate('update'),  UsersController.update);
router.delete('/:userId', UsersController.validate('urlParameter'), UsersController.delete);

module.exports = router;
