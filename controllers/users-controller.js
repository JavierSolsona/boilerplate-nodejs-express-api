const models  = require('../models');
const fs = require('fs');
const { body, param, validationResult } = require('express-validator');
const {
  NAME_LENGTH,
  INTERNAL_SERVER_ERROR,
  BAD_REQUEST,
  ID_INT,
  OBJECT_NOT_FOUND,
  OBJECT_NOT_UPDATED,
  OBJECT_NOT_DELETED
} = require('../constants/errors');
const {
  OBJECT_DELETED
} = require('../constants/success');

exports.validate = (method) => {
  switch (method) {
    case 'create': {
     return [ 
        body('name').isLength({min: 1, max: 255}).withMessage(NAME_LENGTH)
       ]   
    }
    case 'urlParameter': {
      return [
        param('userId').isInt({min: 1}).withMessage(ID_INT)
      ]
    }
    case 'update': {
      return [
        param('userId').isInt({min: 1}).withMessage(ID_INT),
        body('name').isLength({min: 1, max: 255}).withMessage(NAME_LENGTH)
      ]
    }
  }
}

exports.index = (req, res) => {
    models.User.findAll()
        .then((result) => {
            return res.status(200).send(result);
        }).catch((err) => {
            return res.status(500).send({errors: [], message: INTERNAL_SERVER_ERROR});
        });
};

exports.create = (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(400).send({errors: errors.array(), message: BAD_REQUEST});
    }

    models.User.create({name: req.body.name})
        .then((result) => {
            return res.status(201).send(result);
        }).catch((err) => {
            return res.status(500).send({errors: [], message: INTERNAL_SERVER_ERROR});
        });
};

exports.getById = (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).send({errors: errors.array(), message: BAD_REQUEST});
    }

    models.User.findByPk(req.params.userId)
        .then((result) => {
            if (result) {
              return res.status(200).send(result);
            }else {
              return res.status(417).send({errors: [], message: OBJECT_NOT_FOUND});
            }
        }).catch((err) => {
            return res.status(500).send({errors: [], message: INTERNAL_SERVER_ERROR});
        });
};

exports.update = (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).send({errors: errors.array(), message: BAD_REQUEST});
    }

    models.User.update({name: req.body.name}, {where: { id: req.params.userId } })
        .then((modified) => {
            if (modified[0] === 0) {
                return res.status(417).send({errors: [], message: OBJECT_NOT_UPDATED});
            } else {
                return models.User.findByPk(req.params.userId)
            }
        }).then((result) => {
            /*The result isn't verified because if the id not exist
             the updated give the error*/
            return res.status(200).send(result);
        }).catch((err) => {
            return res.status(500).send({errors: [], message: INTERNAL_SERVER_ERROR});
        });
};

exports.delete = (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
      return res.status(400).send({errors: errors.array(), message: BAD_REQUEST});
    }

    models.User.findByPk(req.params.userId)
        .then((result) => {
          if (result) {
            models.User.destroy({where: { id: req.params.userId } })
              .then((deleted) => {
                  if (deleted === 0) {
                    return res.status(417).send({errors: [], message: OBJECT_NOT_DELETED});
                  } else {
                    return res.status(200).send({message: OBJECT_DELETED});
                  }
              }).catch((err) => {
                  return res.status(500).send({errors: [], message: INTERNAL_SERVER_ERROR});
              });
          }else {
            return res.status(417).send({errors: [], message: OBJECT_NOT_FOUND});
          }
        }).catch((err) => {
            return res.status(500).send({errors: [], message: INTERNAL_SERVER_ERROR});
        });
};